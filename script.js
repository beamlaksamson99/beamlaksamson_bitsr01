// dark mode /
const body = document.querySelector("body");
const topHeader = document.querySelector(".top-header");
const headerTitle = document.querySelector(".header-title");
const highlight = document.querySelector(".highlight");
const headermarquee = document.querySelector(".header-text");
const sectionTitleH1 = document.getElementsByClassName("section-title-h1");
const innerSection = document.getElementsByClassName("inner-section");
const textContent = document.getElementsByClassName("text-content");
const textContentRight = document.querySelector(".text-content-right");
const footer = document.getElementById("footer");
const ModeBtn = document.getElementById("darkModeBtn");

ModeBtn.addEventListener("click", changeColor);

function changeColor() {
  // day mode
  if (body.style.backgroundColor == "whitesmoke") {
    body.style.backgroundColor = "#1A2238";
    topHeader.style.backgroundColor = "whitesmoke";
    headerTitle.style.color = "#1A2238";

    Array.from(sectionTitleH1).forEach((each) => {
      each.style.color = "whitesmoke";
    });
    Array.from(innerSection).forEach((each) => {
      each.style.backgroundColor = "#1A2238";
    });
    Array.from(textContent).forEach((each) => {
      each.style.color = "whitesmoke";
      each.style.backgroundColor = "#1A2238";
    });
    textContentRight.style.color = "whitesmoke";
    textContentRight.style.backgroundColor = "#1A2238";
    footer.style.color = "#1A2238";
    footer.style.fontWeight = "bold";
    footer.style.backgroundColor = "whitesmoke";
  }
  // night mode
  else {
    body.style.backgroundColor = "whitesmoke";
    topHeader.style.backgroundColor = "#1A2238";
    headerTitle.style.color = "white";
    Array.from(sectionTitleH1).forEach((each) => {
      each.style.color = "#1A2238";
    });
    Array.from(innerSection).forEach((each) => {
      each.style.backgroundColor = "whitesmoke";
    });
    Array.from(textContent).forEach((each) => {
      each.style.color = "#1A2238";
      each.style.backgroundColor = "whitesmoke";
    });
    textContentRight.style.color = "#1A2238";
    textContentRight.style.backgroundColor = "whitesmoke";
    footer.style.color = "whitesmoke";
    footer.style.backgroundColor = "#1A2238";
  }
}
